//
//  Video.swift
//  Test24i
//
//  Created by Carlos on 26/8/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import Foundation

public struct Video{
    public var name: String?
    public var id: String?
    public var key: String?
    public var iso31661: String?
    public var size: Int?
    public var iso6391: String?
    public var type: String?
    public var site: String?
    
    init(){
        
    }
    
    init(fromEntity entity: VideoNetworkEntity){
        self.name = entity.name
        self.id = entity.id
        self.key = entity.key
        self.iso31661 = entity.iso31661
        self.size = entity.size
        self.iso6391 = entity.iso6391
        self.type = entity.type
        self.site = entity.site
    }
    init(fromRealmEntity entity: VideoRealmEntity){
        self.name = entity.name
        self.id = entity.id
        self.key = entity.key
        self.iso31661 = entity.iso31661
        self.size = entity.size?.value
        self.iso6391 = entity.iso6391
        self.type = entity.type
        self.site = entity.site
    }

}
