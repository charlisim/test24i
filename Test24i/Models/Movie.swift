//
//  Movie.swift
//
//  Created by Carlos on 26/8/17
//  Copyright (c) . All rights reserved.
//

import Foundation

public struct Movie {
    
    // MARK: Properties
    public var posterPath: String?
    public var backdropPath: String?
    public var voteCount: Int?
    public var overview: String?
    public var originalTitle: String?
    public var popularity: Float?
    public var releaseDate: String?
    public var id: Int?
    public var video: Bool? = false
    public var originalLanguage: String?
    public var voteAverage: Float?
    public var title: String?
    public var adult: Bool? = false
    public var videos: [Video]? = []
    public var genres: String?
    init(){
        
    }
    
    init(fromNetworkEntity entity: MovieNetworkEntity){
        self.posterPath = entity.posterPath
        self.backdropPath = entity.backdropPath
        self.voteCount = entity.voteCount
        self.overview = entity.overview
        self.originalTitle = entity.originalTitle
        self.popularity = entity.popularity
        self.releaseDate = entity.releaseDate
        self.id = entity.id
        self.video = entity.video
        self.originalLanguage = entity.originalLanguage
        self.voteAverage = entity.voteAverage
        self.title = entity.title
        self.adult = entity.adult
    }
    
    init(fromRealmEntity entity: MovieRealmEntity){
        self.posterPath = entity.posterPath
        self.backdropPath = entity.backdropPath
        self.voteCount = entity.voteCount?.value
        self.overview = entity.overview
        self.originalTitle = entity.originalTitle
        self.popularity = entity.popularity?.value
        self.releaseDate = entity.releaseDate
        self.id = entity.id
        self.video = entity.video
        self.originalLanguage = entity.originalLanguage
        self.voteAverage = entity.voteAverage?.value
        self.title = entity.title
        self.adult = entity.adult
        self.videos = entity.videos.flatMap({ (entity) -> Video? in
            return Video(fromRealmEntity: entity)
        })
    }
    
    public mutating func addMovieDetail(detail: MovieDetailNetworkEntity){
        self.overview = detail.overview
        if let genres = detail.genres{
            self.genres = (genres.flatMap({ (genre:GenresNetworkEntity) -> String? in
                return genre.name
            }) as NSArray).componentsJoined(by: ",")
        }
        
    }
    
    public mutating func addVideoInfo(videos: MovieVideoResultsNetworkEntity){
        self.videos = videos.results?.flatMap({ (entity) -> Video? in
            return Video(fromEntity: entity)
        })
    }
    
    public func getPosterURL()->String?{
        // TODO: Handle differente poster sizes
        if let posterPath = self.posterPath{
            return "https://image.tmdb.org/t/p/w185" + posterPath
        }
        return nil
    }
    
    public func getYoutubeTrailer() -> URL?{
        return self.videos?.filter({ (video) -> Bool in
            return video.site == "YouTube"
        }).flatMap({ (video) -> URL? in
            return URL(string: "https://www.youtube.com/watch?v=\(video.key!)")
        }).first
    }
}
