// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable file_length

// swiftlint:disable explicit_type_interface identifier_name line_length nesting type_body_length type_name
enum L10n {
  /// Date
  static let date = L10n.tr("Localizable", "date")
  /// Generes
  static let generes = L10n.tr("Localizable", "generes")
  /// Movie Detail
  static let movieDetailTitle = L10n.tr("Localizable", "movie_detail_title")
  /// Movie Catalog
  static let movieListTitle = L10n.tr("Localizable", "movie_list_title")
  /// No movies available
  static let noMoviesAvailable = L10n.tr("Localizable", "no_movies_available")
  /// Overwiew
  static let overview = L10n.tr("Localizable", "overview")
  /// Check your connection and try again
  static let trailerNotAvailableMessage = L10n.tr("Localizable", "trailer_not_available_message")
  /// Trailer not available
  static let trailerNotAvailableTitle = L10n.tr("Localizable", "trailer_not_available_title")
  /// Watch trailer
  static let watchTrailerTitle = L10n.tr("Localizable", "watch_trailer_title")
}
// swiftlint:enable explicit_type_interface identifier_name line_length nesting type_body_length type_name

extension L10n {
  fileprivate static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}

