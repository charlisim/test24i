//
//  MoviesService.swift
//  Test24i
//
//  Created by Carlos on 26/8/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import Foundation
import RxSwift
import ObjectMapper
import Realm
import RealmSwift

public protocol MovieService{
    func getPopularMovies()->Observable<[Movie]>
    func getDetailForMovie(_ movie:Movie)->Observable<Movie>
    func getVideosForMovie(_ movie:Movie)->Observable<Movie>
}
public protocol MoviePresistenceService: MovieService{
    func saveMovies(movies:[Movie])
    func saveMovie(movie:Movie)
}

public class MoviesNetworkService:MovieService{
    let networkService: NetworkService
    let configService: ConfigService
    
    init(networkService: NetworkService, configService: ConfigService){
        self.networkService = networkService
        self.configService = configService
    }
    
    public func getPopularMovies()->Observable<[Movie]>{
        let url = URL(string: "https://api.themoviedb.org/3/movie/popular?api_key=" + configService.apiKey)
        return self.networkService.doGetRequest(url: url!).flatMap({ (response:NetworkService.NetworkResponse) -> Observable<MovieNetworkEntity> in
            if let data = response.data{
                let result: ResultNetworkEntity? = Mapper<ResultNetworkEntity>().map(JSONObject: data)
                let movies:[MovieNetworkEntity]? = Mapper<MovieNetworkEntity>().mapArray(JSONObject: result?.results)
                if let movies = movies{
                    return Observable.from(movies)
                }
                return Observable.empty()
            }
            return Observable.empty()
        }).map({ (entity) -> Movie in
            return Movie(fromNetworkEntity: entity)
        }).toArray()
    }
    
    public func getDetailForMovie(_ movie:Movie)->Observable<Movie>{
        var mutableMovie = movie
        let url = URL(string: "https://api.themoviedb.org/3/movie/\(movie.id!)?api_key=" + configService.apiKey)
        return self.networkService.doGetRequest(url: url!).flatMap({ (response:NetworkService.NetworkResponse) -> Observable<MovieDetailNetworkEntity> in
            if let data = response.data{
                let movie:MovieDetailNetworkEntity? = Mapper<MovieDetailNetworkEntity>().map(JSONObject: data)
                if let movie = movie{
                    return Observable.from(movie)
                }
                return Observable.empty()
            }
            return Observable.empty()
        }).map({ (entity) -> Movie in
            mutableMovie.addMovieDetail(detail: entity)
            return mutableMovie
        })
    }
    
    public func getVideosForMovie(_ movie:Movie)->Observable<Movie>{
        var mutableMovie = movie
        let url = URL(string: "https://api.themoviedb.org/3/movie/\(movie.id!)/videos?api_key=" + configService.apiKey)
        return self.networkService.doGetRequest(url: url!).flatMap({ (response:NetworkService.NetworkResponse) -> Observable<MovieVideoResultsNetworkEntity> in
            if let data = response.data{
                let movie:MovieVideoResultsNetworkEntity? = Mapper<MovieVideoResultsNetworkEntity>().map(JSONObject: data)
                if let movie = movie{
                    return Observable.from(movie)
                }
                return Observable.empty()
            }
            return Observable.empty()
        }).map({ (entity) -> Movie in
            mutableMovie.addVideoInfo(videos: entity)
            return mutableMovie
        })
        
    }
    
}

public class MoviesRealmService: MoviePresistenceService{
    
    public func getPopularMovies() -> Observable<[Movie]> {
        return Observable.create({ (observer:AnyObserver<Movie>) -> Disposable in
            let realm = try! Realm()
            let movies = realm.objects(MovieRealmEntity.self).flatMap { (entity) -> Movie? in
                return Movie(fromRealmEntity: entity)
            }
            movies.forEach({ (movie) in
                observer.onNext(movie)
            })
            observer.onCompleted()
            return Disposables.create()
        }).toArray()
        
    }
    /**
     Returns same object passed by parameter. In offline mode there is no way to get extra details of Movie.
     */
    public func getDetailForMovie(_ movie: Movie) -> Observable<Movie> {
        return Observable.just(movie)
    }
    /**
     Returns same object passed by parameter. In offline mode there is no way to get extra videos of Movie.
     */
    public func getVideosForMovie(_ movie: Movie) -> Observable<Movie> {
        return Observable.just(movie)
    }
    
    public func saveMovies(movies:[Movie]){
        let realm = try! Realm()
        let moviesRealm = movies.flatMap { (movie) -> MovieRealmEntity? in
            return MovieRealmEntity(model: movie)
        }
        try! realm.write {
            realm.add(moviesRealm, update: true)
        }
    }
    
    public func saveMovie(movie:Movie){
        self.saveMovies(movies: [movie])
    }
}
