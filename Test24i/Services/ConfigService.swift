//
//  ConfigService.swift
//  Test24i
//
//  Created by Carlos on 26/8/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import Foundation


public class ConfigService{
    
    var apiKey: String {
        get {
            if let path = Bundle.main.path(forResource: "Info", ofType: "plist"), let dict = NSDictionary(contentsOfFile: path) {
                return dict.value(forKey: "API_KEY") as? String ?? ""
            }
            return ""
        }
    }
    
}
