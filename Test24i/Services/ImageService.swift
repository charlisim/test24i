//
//  ImageService.swift
//  Test24i
//
//  Created by Carlos on 26/8/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

public class ImageService{
    
    func downloadImageAt(url:String)->Observable<UIImage>{
        return Observable.create({ (observer) -> Disposable in
            Alamofire.request(url).responseData { response in
                if let data = response.result.value, let image = UIImage(data: data) {
                    observer.onNext(image)
                    observer.onCompleted()
                }
                if let error = response.error{
                    observer.onError(error)
                }
            
            }.resume()
            return Disposables.create()
        })
    }
    
}
