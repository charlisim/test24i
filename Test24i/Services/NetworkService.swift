//
//  NetworkService.swift
//  Test24i
//
//  Created by Carlos on 26/8/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire
public class NetworkService{
    public struct NetworkResponse{
        public let data:Any?
        public let response: URLResponse?
    }
    
    let configuration: URLSessionConfiguration = URLSessionConfiguration()
    let session: URLSession = URLSession()
    
    func doGetRequest(url: URL)->Observable<NetworkResponse>{
        return Observable.create({ (observer) -> Disposable in
            Alamofire.request(url).responseJSON { response in
        
                if let error = response.error{
                    observer.onError(error)
                    return
                }
                observer.onNext(NetworkService.NetworkResponse(data: response.result.value, response: response.response))
                observer.onCompleted()
            }
            return Disposables.create()
        })
        
    }
    
}
