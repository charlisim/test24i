//
//  GetMovies
//  Test24i
//
//  Created by Carlos on 26/8/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import Foundation
import RxSwift

public class GetMovies{
    private let networkService: MovieService
    private let realmService: MoviePresistenceService
    
    init(networkService:MovieService, realmService: MoviePresistenceService){
        self.networkService = networkService
        self.realmService = realmService
    }
    
    func execute()->Observable<[Movie]>{
        return self.networkService.getPopularMovies().catchError({ (error) -> Observable<[Movie]> in
            return self.realmService.getPopularMovies()
        }).do(onNext: { (movies) in
            self.realmService.saveMovies(movies: movies)
        })
    }
}
