//
//  SearchMovies.swift
//  Test24i
//
//  Created by Carlos on 26/8/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import Foundation

public class SearchMovies{
    private var allMovies:[Movie] = []
    
    func setMovies(movies:[Movie]){
        self.allMovies = movies
    }
    
    func searchByTitle(text:String)->[Movie]{
        if (text.isEmpty){
            return allMovies
        }
        return self.allMovies.filter { (movie) -> Bool in
            return movie.title?.contains(text) ?? false
        }
    }
    
}
