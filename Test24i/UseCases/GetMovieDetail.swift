//
//  GetMovieDetail.swift
//  Test24i
//
//  Created by Carlos on 27/8/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import Foundation
import RxSwift

public class GetMovieDetail{
    private let networkService: MovieService
    private let realmService: MoviePresistenceService
    
    init(networkService:MovieService, realmService: MoviePresistenceService){
        self.networkService = networkService
        self.realmService = realmService
    }
    
    func execute(movie:Movie)->Observable<Movie>{
        return self.networkService.getDetailForMovie(movie).catchError({ (error) -> Observable<Movie> in
            return self.realmService.getVideosForMovie(movie)
        }).do(onNext: { (movie) in
            self.realmService.saveMovie(movie: movie)
        })
    }
}
