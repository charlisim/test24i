//
//  MovieListBuilder.swift
//  Test24i
//
//  Created by Carlos on 26/8/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import UIKit


public class MovieListBuilder{
    
    func build(output:MovieListPresenterOutputProtocol)->UIViewController{
        let vc: MovieListViewController = MovieListViewController()
        let presenter: MovieListPresenter = MovieListPresenter(view: vc, getMovies: UseCaseBuilder.sharedInstance.buildGetMovies(), searchMovies: UseCaseBuilder.sharedInstance.buildSearchMovies(), output: output)
        vc.presenter = presenter
        return vc
    }
    
    func buildMovieCell(forTableView tableView: UITableView, forIndexPath indexPath:IndexPath, movie:Movie)->UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: MovieListViewController.reuseIdentifier, for: indexPath)
        if let movieCell = cell as? MovieCell{
            let presenter = MovieCellPresenter(movie: movie, view: movieCell, imageService: ServicesBuilder.sharedInstance.buildImageService())
            movieCell.presenter = presenter
        }
        return cell
    }
    
}
