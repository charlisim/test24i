//
//  ServicesBuilder.swift
//  Test24i
//
//  Created by Carlos on 26/8/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import Foundation

// TODO: Handle reusability and no build each service every time build methods are called
public class ServicesBuilder{
    
    static let sharedInstance: ServicesBuilder = ServicesBuilder()
    
    func buildMovieNetworkService()->MoviesNetworkService{
        return MoviesNetworkService(networkService: self.buildNetworkService(), configService: self.buildConfigService())
    }
    func buildMovieRealmService()->MoviesRealmService{
        return MoviesRealmService()
    }
    func buildNetworkService()->NetworkService{
        return NetworkService()
    }
    func buildConfigService()->ConfigService{
        return ConfigService()
    }
    func buildImageService()->ImageService{
        return ImageService()
    }
    
}
