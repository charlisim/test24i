//
//  UseCasesBuilder.swift
//  Test24i
//
//  Created by Carlos on 26/8/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import Foundation

public class UseCaseBuilder{
    static let sharedInstance: UseCaseBuilder = UseCaseBuilder()
    
    public func buildGetMovies()->GetMovies{
        return GetMovies(networkService: ServicesBuilder.sharedInstance.buildMovieNetworkService(), realmService: ServicesBuilder.sharedInstance.buildMovieRealmService())
    }
    public func buildGetMovieDetail()->GetMovieDetail{
        return GetMovieDetail(networkService: ServicesBuilder.sharedInstance.buildMovieNetworkService(), realmService: ServicesBuilder.sharedInstance.buildMovieRealmService())
    }
    public func buildGetMovieVideos()->GetMovieVideos{
        return GetMovieVideos(networkService: ServicesBuilder.sharedInstance.buildMovieNetworkService(), realmService: ServicesBuilder.sharedInstance.buildMovieRealmService())
    }
    public func buildSearchMovies()->SearchMovies{
        return SearchMovies()
    }
}
