//
//  MovieDetailBuilder.swift
//  Test24i
//
//  Created by Carlos on 27/8/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import UIKit


public class MovieDetailBuilder{
    
    func build(movie:Movie, output: MovieDetailOutputProtocol)->UIViewController{
        let vc = MovieDetailViewController()
        let presenter = MovieDetailPresenter(movie: movie, view: vc, imageService: ServicesBuilder.sharedInstance.buildImageService(), getMovieDetail: UseCaseBuilder.sharedInstance.buildGetMovieDetail(), getVideos: UseCaseBuilder.sharedInstance.buildGetMovieVideos(), output: output)
        vc.presenter = presenter
        return vc
    }
    
}
