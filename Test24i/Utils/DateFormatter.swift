//
//  DateFormatter.swift
//  Test24i
//
//  Created by Carlos on 27/8/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import Foundation

public class DateFormatters{
    
    private func stringDateToStringWithSeparator(_ separator: String, date:String)->String {
        return date.replacingOccurrences(of: "-", with: separator)
    }
    
    public func dateWithDot(date:String)->String {
        return self.stringDateToStringWithSeparator(".", date: date)
    }
    
}
