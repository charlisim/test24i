//
//  TitleDetailView.swift
//  Test24i
//
//  Created by Carlos on 27/8/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import UIKit
import UIBase


public class TitleDetailView: UIView, SetupUI{
    
    private let titleLabel:UILabel = UILabel()
    private let detailLabel: UILabel = UILabel()
    
    public init(title:String){
        super.init(frame: CGRect.zero)
        self.titleLabel.text = title
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func didMoveToWindow() {
        if self.window != nil{
            self.setupViews()
            self.setupConstraints()
        }
    }
    
    public func setupViews() {
        self.addSubview(titleLabel)
        self.addSubview(detailLabel)
        
        titleLabel.font = UIFont.systemFont(ofSize: 13, weight: UIFontWeightBold)
        detailLabel.font = UIFont.systemFont(ofSize: 13, weight: UIFontWeightRegular)
        
        detailLabel.numberOfLines = 0

    }
    public func setupConstraints() {
        self.titleLabel.topAnchor == self.topAnchor
        self.titleLabel.leadingAnchor == self.leadingAnchor
        self.titleLabel.trailingAnchor == self.trailingAnchor
        self.titleLabel.heightAnchor == 14
        
        self.detailLabel.topAnchor == self.titleLabel.bottomAnchor + 3
        self.detailLabel.horizontalAnchors == self.titleLabel.horizontalAnchors
        self.detailLabel.bottomAnchor == self.bottomAnchor
    }
    
    public func setup(title: String, detail:String){
        self.titleLabel.text = title
        self.detailLabel.text = detail
    }
    public func setup(title: String){
        self.titleLabel.text = title
    }
    
    public func setup(detail: String){
        self.detailLabel.text = detail
    }
    
}
