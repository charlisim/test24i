//
//  MovieDetailView.swift
//  Test24i
//
//  Created by Carlos on 27/8/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import UIKit
import UIBase

public protocol MovieDetailView: UIBaseView{
    func setPosterImage(_ image:UIImage)
    func setMovieTitle(_ title: String)
    func setTrailerButtonVisibility(visible: Bool)
    func setGeneres(_ generes:String)
    func setReleaseDate(_ date:String)
    func setOverview(_ overview:String)
    func setupTitle(_ title: String)
    func showVideoNoAvailable()
}

public class MovieDetailViewController: UIViewController, SetupUI, MovieDetailView{
    var presenter: MovieDetailPresenter? = nil
    
    // MARK: Views
    private let scrollView: UIScrollView = UIScrollView()
    private let stackView: UIStackView = UIStackView()
    private let topStackView: UIStackView = UIStackView()
    private let topTitleButtonStackView: UIStackView = UIStackView()
    
    private let bottomStackView: UIStackView = UIStackView()
    private let posterImageView: UIImageView = UIImageView()
    private let movieTitleLabel: UILabel = UILabel()
    private let watchTrailerButton: UIButton = UIButton()
    private let generesBlock = TitleDetailView(title: L10n.generes)
    private let releaseDateBlock = TitleDetailView(title: L10n.date)
    private let overviewBlock = TitleDetailView(title: L10n.overview)
    
    
    
    // MARK: MovieDetailView
    
    public func setPosterImage(_ image:UIImage){
        self.posterImageView.image = image
    }
    public func setMovieTitle(_ title: String){
        self.movieTitleLabel.text = title
    }
    public func setTrailerButtonVisibility(visible: Bool){
        if !visible{
            self.stackView.removeArrangedSubview(watchTrailerButton)
        }
    }
    public func setGeneres(_ generes:String){
        self.generesBlock.setup(detail: generes)
        
    }
    public func setReleaseDate(_ date:String){
        self.releaseDateBlock.setup(detail: date)
    }
    public func setOverview(_ overview:String){
        self.overviewBlock.setup(detail: overview)
    }
    public func setupTitle(_ title: String) {
        self.title = title
        
    }
    public func showVideoNoAvailable() {
        let alertVC = UIAlertController(title: L10n.trailerNotAvailableTitle, message: L10n.trailerNotAvailableMessage, preferredStyle: UIAlertControllerStyle.alert)
        alertVC.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alertVC, animated: true, completion: nil)
    }
    
    // MARK: Setup UI
    public func setupViews(){
        self.view.backgroundColor = UIColor.white
        self.view.addSubview(scrollView)
        self.scrollView.addSubview(stackView)
        self.stackView.addArrangedSubview(topStackView)
        self.stackView.addArrangedSubview(bottomStackView)
        self.topStackView.addArrangedSubview(posterImageView)
        self.topStackView.addArrangedSubview(topTitleButtonStackView)
        self.topTitleButtonStackView.addArrangedSubview(movieTitleLabel)
        self.topTitleButtonStackView.addArrangedSubview(watchTrailerButton)
        self.bottomStackView.addArrangedSubview(generesBlock)
        self.bottomStackView.addArrangedSubview(releaseDateBlock)
        self.bottomStackView.addArrangedSubview(overviewBlock)
        
        self.setPortraitStackView()
        
        self.posterImageView.contentMode = .scaleAspectFill
        self.posterImageView.clipsToBounds = true
        
        watchTrailerButton.setTitle(L10n.watchTrailerTitle, for: .normal)
        watchTrailerButton.backgroundColor = UIColor.lightGray
        watchTrailerButton.setTitleColor(UIColor.black, for: .normal)
        
        movieTitleLabel.font = UIFont.boldSystemFont(ofSize: 20)
    }
    public func setupConstraints(){
        self.scrollView.topAnchor == self.view.topAnchor
        self.scrollView.horizontalAnchors == self.view.horizontalAnchors
        self.scrollView.bottomAnchor == self.bottomLayoutGuide.bottomAnchor
        self.stackView.edgeAnchors == self.scrollView.edgeAnchors
        self.stackView.widthAnchor == self.scrollView.widthAnchor
        self.posterImageView.heightAnchor == 200
        self.watchTrailerButton.heightAnchor == 44
        
        
    }
    public func setupActions(){
        self.watchTrailerButton.addTarget(self, action: #selector(MovieDetailViewController.didTapWatchTrailer), for: .touchUpInside)
        
    }
    
    func didTapWatchTrailer(){
        self.presenter?.didTapShowTrailer()
    }
    private func setPortraitStackView(){
        self.stackView.alignment = .fill
        self.stackView.axis = .vertical
        self.stackView.distribution = .equalCentering
        self.stackView.spacing = 5
        
        self.topStackView.alignment = .fill
        self.topStackView.axis = .vertical
        self.topStackView.distribution = .fill
        self.topStackView.spacing = 0
        
        self.topTitleButtonStackView.alignment = .fill
        self.topTitleButtonStackView.axis = .vertical
        self.topTitleButtonStackView.distribution = .fill
        self.topTitleButtonStackView.spacing = 10
        
        self.bottomStackView.alignment = .fill
        self.bottomStackView.axis = .vertical
        self.bottomStackView.distribution = .equalSpacing
        self.bottomStackView.spacing = 20
        
        self.stackView.layoutMargins = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        self.stackView.isLayoutMarginsRelativeArrangement = true

        
        self.topTitleButtonStackView.layoutMargins = UIEdgeInsets(top: 10, left: 10, bottom: 0, right: 10)
        self.topTitleButtonStackView.isLayoutMarginsRelativeArrangement = true
        
        self.bottomStackView.layoutMargins = UIEdgeInsets(top: 10, left: 10, bottom: 0, right: 10)
        self.bottomStackView.isLayoutMarginsRelativeArrangement = true

        
    }
    private func setLandscapeStackView(){
        self.stackView.alignment = .fill
        self.stackView.axis = .vertical
        self.stackView.distribution = .equalSpacing
        self.stackView.spacing = 5
        
        self.topStackView.alignment = .fill
        self.topStackView.axis = .horizontal
        self.topStackView.distribution = .fillEqually
        self.topStackView.spacing = 5
        
        self.topTitleButtonStackView.alignment = .fill
        self.topTitleButtonStackView.axis = .vertical
        self.topTitleButtonStackView.distribution = .equalCentering
        self.topTitleButtonStackView.spacing = 5
        
        self.bottomStackView.alignment = .fill
        self.bottomStackView.axis = .vertical
        self.bottomStackView.distribution = .equalSpacing
        self.bottomStackView.spacing = 20
        
        self.topTitleButtonStackView.layoutMargins = UIEdgeInsets(top: 10, left: 5, bottom: 0, right: 10)
        self.topTitleButtonStackView.isLayoutMarginsRelativeArrangement = true
        
        self.bottomStackView.layoutMargins = UIEdgeInsets(top: 10, left: 10, bottom: 0, right: 10)
        self.bottomStackView.isLayoutMarginsRelativeArrangement = true

    }
    // MARK: VC Lifecycle
    
    public override func viewDidLoad() {
        self.presenter?.didLoadView()
    }
    public override func viewDidAppear(_ animated: Bool) {
        self.presenter?.onViewAppeared()
    }
    public override func viewDidDisappear(_ animated: Bool) {
        self.presenter?.onViewDissapear()
    }
    public override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        if self.traitCollection.verticalSizeClass == .compact{
            self.setLandscapeStackView()
        }else{
            self.setPortraitStackView()
        }
    }
    
}
