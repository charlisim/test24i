//
//  MovieDetailPresenter.swift
//  Test24i
//
//  Created by Carlos on 27/8/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import Foundation
import UIBase
import RxSwift

public protocol MovieDetailOutputProtocol{
    func previewTrailerWithId(_ id:String)
}

public class MovieDetailPresenter:UIBasePresenter{
    private weak var view: MovieDetailView? = nil
    private var output: MovieDetailOutputProtocol
    private let imageService:ImageService
    private let getMovieDetail: GetMovieDetail
    private let getMovieVideos: GetMovieVideos
    
    private var movie:Movie
    private let disposeBag: DisposeBag = DisposeBag()
    
    public init(movie:Movie, view: MovieDetailView, imageService: ImageService, getMovieDetail: GetMovieDetail, getVideos:GetMovieVideos, output: MovieDetailOutputProtocol){
        self.movie = movie
        self.view = view
        self.imageService = imageService
        self.getMovieDetail = getMovieDetail
        self.getMovieVideos = getVideos
        self.output = output
    }
    
    
    // MARK : UIBasePresenter
    public func didLoadView() {
        self.view?.setupView()
        self.view?.setupTitle(L10n.movieDetailTitle)
        self.getMovieDetail.execute(movie: movie).subscribe(onNext: { (movie) in
            self.movie = movie
            self.setupMovieInfo(movie: movie)
        }).addDisposableTo(disposeBag)
    }
    private func setupMovieInfo(movie: Movie){
        self.view?.setMovieTitle(movie.title ?? "")
        self.view?.setOverview(movie.overview ?? "")
        if let date = movie.releaseDate{
            self.view?.setReleaseDate(DateFormatters().dateWithDot(date: date))
        }
        self.view?.setGeneres(movie.genres ?? "")
        if let posterPath = movie.getPosterURL(){
            self.imageService.downloadImageAt(url: posterPath).subscribe(onNext: { (image) in
                self.view?.setPosterImage(image)
            }).addDisposableTo(disposeBag)
            
        }
        
    }
    public func onViewAppeared(){
        
    }
    public func onViewDissapear(){
        
    }
    
    func didTapShowTrailer(){
        self.getMovieVideos.execute(movie: movie).subscribe(onNext: { (movie) in
            self.movie = movie
            if let trailerKey = self.movie.videos?.first?.key{
                self.output.previewTrailerWithId(trailerKey)
            }else{
                self.view?.showVideoNoAvailable()
            }
        }, onError: { (error) in
            self.view?.showVideoNoAvailable()
        }).addDisposableTo(disposeBag)
    }

}
