//
//  MovieListPresenter.swift
//  Test24i
//
//  Created by Carlos on 26/8/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import Foundation
import UIBase
import RxSwift

public protocol MovieListPresenterOutputProtocol{
    func navigateToDetail(movie:Movie)
}

class MovieListPresenter: UIBasePresenter{
    weak var view: MovieListView? = nil
    var output: MovieListPresenterOutputProtocol? = nil
    private let getMovies:GetMovies
    private let searchMovies: SearchMovies
    private let disposeBag = DisposeBag()
    
    init(view: MovieListView, getMovies: GetMovies, searchMovies: SearchMovies, output:MovieListPresenterOutputProtocol){
        self.view = view
        self.getMovies = getMovies
        self.searchMovies = searchMovies
        self.output = output
    }
    
    
    func didLoadView(){
        self.view?.setupView()
        self.view?.setupTitle(title: L10n.movieListTitle)
        self.getMovies.execute().subscribe(onNext: { (movies) in
            if movies.isEmpty{
                self.view?.showEmptyMoviesView()
            }else{
                self.view?.didReceiveMovieList(movies: movies)
                self.searchMovies.setMovies(movies: movies)
            }
        }, onError: { (error) in
            self.view?.showEmptyMoviesView()
        }).addDisposableTo(disposeBag)

    }
    func onViewAppeared(){
        
    }
    func onViewDissapear(){
        
    }
    
    func didSearch(term:String){
        let filteredMovies = self.searchMovies.searchByTitle(text: term)
        self.view?.didReceiveMovieList(movies: filteredMovies)
    }
    
    func didSelectMovie(movie:Movie){
        self.output?.navigateToDetail(movie: movie)
    }
    
}
