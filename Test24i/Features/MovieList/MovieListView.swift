//
//  ViewController.swift
//  Test24i
//
//  Created by Carlos on 26/8/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import UIKit
import UIBase


public protocol MovieListView: UIBaseView{
    func didReceiveMovieList(movies:[Movie])
    func showEmptyMoviesView()
    func setupTitle(title:String)
}

public class MovieListViewController: UIViewController, MovieListView, SetupUI {
    static let reuseIdentifier: String = "movie-cell"
    
    var presenter: MovieListPresenter? = nil
    private let tableView: UITableView = UITableView()
    private let emptyLabel: UILabel = UILabel()
    private let searchBar: UISearchBar = UISearchBar()
    var movies: [Movie] = []
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.presenter?.didLoadView()
    }
    
    public func setupViews() {
        self.view.backgroundColor = UIColor.white
        self.view.addSubview(self.emptyLabel)
        self.view.addSubview(self.tableView)
        self.view.addSubview(self.searchBar)
        self.tableView.register(MovieCell.self, forCellReuseIdentifier: MovieListViewController.reuseIdentifier)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.keyboardDismissMode = .interactive
        self.searchBar.delegate = self
        automaticallyAdjustsScrollViewInsets = false
        definesPresentationContext = true
        
    }
    
    public func setupConstraints() {
        
        
        // Search Bar
        
        self.searchBar.topAnchor == self.topLayoutGuide.bottomAnchor
        self.searchBar.horizontalAnchors == self.view.horizontalAnchors
        self.searchBar.heightAnchor == 44
        self.searchBar.inputAccessoryView = UIView()
        
        // Table View
        self.tableView.topAnchor == self.searchBar.bottomAnchor
        self.tableView.horizontalAnchors == self.view.horizontalAnchors
        self.tableView.bottomAnchor == self.bottomLayoutGuide.bottomAnchor

        
        // Empty Label
        
        self.emptyLabel.edgeAnchors == self.view.edgeAnchors
        
        
    }
    public func setupActions() {
        
    }
    public override func viewDidAppear(_ animated: Bool) {
        self.presenter?.onViewAppeared()

    }
    
    // MARK : MovieListView
    public func setupView() {
        self.setupViews()
        self.setupConstraints()
        self.setupActions()
    }

    public func didReceiveMovieList(movies:[Movie]) {
        self.movies = movies
        self.tableView.isHidden = false
        self.searchBar.isHidden = false
        self.emptyLabel.isHidden = true
        self.tableView.reloadData()
        
    }
    public func showEmptyMoviesView() {
        self.emptyLabel.text = L10n.noMoviesAvailable
        self.emptyLabel.textAlignment = .center
        self.tableView.isHidden = true
        self.searchBar.isHidden = true
        self.emptyLabel.isHidden = false
    }
    public func setupTitle(title: String) {
        self.title = title
        
    }
    func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.view.frame.origin.y -= keyboardSize.height
            self.view.layoutIfNeeded()
        }
    }
    
    func keyboardWillHide(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            self.view.frame.origin.y += keyboardSize.height
            self.view.layoutIfNeeded()
        }
    }
}

extension MovieListViewController: UITableViewDelegate, UITableViewDataSource{
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row < self.movies.count{
            self.presenter?.didSelectMovie(movie: self.movies[indexPath.row])
        }
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row < self.movies.count{
            return MovieListBuilder().buildMovieCell(forTableView: tableView, forIndexPath: indexPath, movie: self.movies[indexPath.row])
        }
        return UITableViewCell()
    }
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.movies.count
    }
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let movieCell = cell as? MovieCell{
            movieCell.presenter?.onViewAppeared()
        }
    }
    public func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let movieCell = cell as? MovieCell{
            movieCell.presenter?.onViewDissapear()
        }
    }
}

extension MovieListViewController: UISearchBarDelegate{
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.presenter?.didSearch(term: searchText)
    }
}
