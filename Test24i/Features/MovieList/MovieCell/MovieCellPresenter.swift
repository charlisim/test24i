//
//  MovieCellPresenter.swift
//  Test24i
//
//  Created by Carlos on 26/8/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import Foundation
import UIBase
import RxSwift

public class MovieCellPresenter: UIBasePresenter{
    var view: MovieCellView?
    private let imageService:ImageService
    private let movie: Movie
    private let disposeBag: DisposeBag = DisposeBag()
    
    init(movie: Movie, view: MovieCellView, imageService: ImageService){
        self.movie = movie
        self.view = view
        self.imageService = imageService
    }
    public func didLoadView(){
        
    }
    public func onViewAppeared(){
        self.view?.setupView()
        if let posterPath = self.movie.getPosterURL(){
            self.imageService.downloadImageAt(url: posterPath).subscribe(onNext: { (image) in
                self.view?.setImage(image: image)
            }).addDisposableTo(disposeBag)
        }
        if let title = self.movie.title{
            self.view?.setTitle(title: title)
        }
        
    }
    public func onViewDissapear(){
        
    }
    
    
}
