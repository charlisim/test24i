//
//  MovieCell.swift
//  Test24i
//
//  Created by Carlos on 26/8/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import UIKit
import UIBase

protocol MovieCellView:UIBaseView{
    func setImage(image:UIImage)
    func setTitle(title:String)
}

public class MovieCell: UITableViewCell, SetupUI, MovieCellView{
    var presenter: MovieCellPresenter? = nil
    let moviePosterImageView: UIImageView = UIImageView()
    let movieTitleLabel: UILabel = UILabel()
    
    public func setupView(){
        self.setupViews()
        self.setupConstraints()
    }
    
    public func setupViews() {
        self.moviePosterImageView.contentMode = .scaleAspectFill
        self.moviePosterImageView.clipsToBounds = true
        
        self.contentView.addSubview(self.moviePosterImageView)
        self.contentView.addSubview(self.movieTitleLabel)
        
        self.movieTitleLabel.numberOfLines = 2
        self.movieTitleLabel.font = UIFont.systemFont(ofSize: 20, weight: UIFontWeightUltraLight)
    }
    public func setupConstraints() {
        moviePosterImageView.leadingAnchor == self.contentView.leadingAnchor
        moviePosterImageView.verticalAnchors == self.contentView.verticalAnchors
        moviePosterImageView.widthAnchor == self.contentView.widthAnchor / 3
        
        movieTitleLabel.leadingAnchor == self.moviePosterImageView.trailingAnchor + 20
        movieTitleLabel.centerYAnchor == self.contentView.centerYAnchor
        movieTitleLabel.trailingAnchor == self.contentView.trailingAnchor - 20
    }
    
    func setImage(image: UIImage){
        self.moviePosterImageView.image = image
    }
    func setTitle(title: String){
        self.movieTitleLabel.text = title
    }
    
    
}
