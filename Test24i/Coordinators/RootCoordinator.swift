//
//  RootCoordinator.swift
//  Test24i
//
//  Created by Carlos on 26/8/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import XCDYouTubeKit

public class RootCoordinator{
    var window: UIWindow
    var navigationController: UINavigationController = UINavigationController()
    var splitView = UISplitViewController()
    
    public init(window: UIWindow){
        self.window = window
    }
    public func start(){
        let vc = MovieListBuilder().build(output: self)
        let showVC:UIViewController
        
        if UIDevice.current.userInterfaceIdiom == .pad{
            self.splitView.preferredDisplayMode = .allVisible
            self.splitView.viewControllers = [vc]
            showVC = self.splitView
        }else{
            self.navigationController.navigationBar.titleTextAttributes =
                [NSFontAttributeName: UIFont.systemFont(ofSize: 20, weight: UIFontWeightLight)]
            self.navigationController.viewControllers = [vc]
            showVC = self.navigationController
        }
        
        self.window.rootViewController = showVC
        self.window.makeKeyAndVisible()
    }
}

extension RootCoordinator: MovieListPresenterOutputProtocol{
    
    public func navigateToDetail(movie: Movie) {
        
        let vc = MovieDetailBuilder().build(movie: movie, output: self)
        if UIDevice.current.userInterfaceIdiom == .pad{
            self.navigationController.viewControllers = [vc]
            self.splitView.showDetailViewController(self.navigationController, sender: nil)
        }else{
            self.navigationController.pushViewController(vc, animated: true)
        }
    }
}
extension RootCoordinator: MovieDetailOutputProtocol{
    public func previewTrailerWithId(_ id:String){
        let vc = XCDYouTubeVideoPlayerViewController(videoIdentifier: id)
    
        self.navigationController.present(vc, animated: true, completion: {
            vc.moviePlayer.play()
            
        })
        
    }
}
