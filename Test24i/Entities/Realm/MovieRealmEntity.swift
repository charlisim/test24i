//
//  MovieRealmEntity.swift
//  Test24i
//
//  Created by Carlos on 27/8/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

public class MovieRealmEntity: Object{
    
    public dynamic var posterPath: String?
    public dynamic var backdropPath: String?
    public var voteCount: RealmOptional<Int>?
    public dynamic var overview: String?
    public dynamic var originalTitle: String?
    public var popularity: RealmOptional<Float>?
    public dynamic var releaseDate: String?
    public var id: Int = 0
    public var video: Bool? = false
    public dynamic var originalLanguage: String?
    public var voteAverage: RealmOptional<Float>?
    public dynamic var title: String?
    public var adult: Bool = false
    public var videos: List<VideoRealmEntity> = List<VideoRealmEntity>()
    public dynamic var genres: String?
    
    required public init(){
        super.init()
    }
    public init(model: Movie){
        super.init()
        self.posterPath = model.posterPath
        self.backdropPath = model.backdropPath
        self.voteCount = RealmOptional(model.voteCount)
        self.overview = model.overview
        self.originalTitle = model.originalTitle
        self.popularity = RealmOptional(model.popularity)
        self.releaseDate = model.releaseDate
        self.id = model.id!
        self.video = model.video
        self.originalLanguage = model.originalLanguage
        self.voteAverage = RealmOptional(model.voteAverage)
        self.title = model.title
        self.adult = model.adult ?? false
        self.videos = List.init()
        let mapped = model.videos?.flatMap({ (video) -> VideoRealmEntity? in
            VideoRealmEntity(fromModel: video)
        })
        if let mappedVideos = mapped{
            self.videos.append(contentsOf: mappedVideos)
        }
        self.genres = model.genres
        
    }
    
    required public init(value: Any, schema: RLMSchema) {
        fatalError("init(value:schema:) has not been implemented")
    }
    
    required public init(realm: RLMRealm, schema: RLMObjectSchema) {
        fatalError("init(realm:schema:) has not been implemented")
    }
    override static public func primaryKey() -> String? {
        return "id"
    }

    
}

