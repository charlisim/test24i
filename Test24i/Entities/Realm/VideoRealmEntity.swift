//
//  VideoRealmEntity.swift
//  Test24i
//
//  Created by Carlos on 27/8/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

public class VideoRealmEntity:Object{
    public dynamic var name: String?
    public dynamic var id: String?
    public dynamic var key: String?
    public dynamic var iso31661: String?
    public var size: RealmOptional<Int>?
    public dynamic var iso6391: String?
    public dynamic var type: String?
    public dynamic var site: String?
    
    public required init(){
        super.init()
    }
    
    public init(fromModel model: Video){
        super.init()
        self.name = model.name
        self.id = model.id
        self.key = model.key
        self.iso31661 = model.iso31661
        self.size = RealmOptional(model.size)
        self.iso6391 = model.iso6391
        self.type = model.type
        self.site = model.site
    }
    
    public required init(realm: RLMRealm, schema: RLMObjectSchema) {
        fatalError("init(realm:schema:) has not been implemented")
    }
    
    required public init(value: Any, schema: RLMSchema) {
        fatalError("init(value:schema:) has not been implemented")
    }
    override public static func primaryKey() -> String? {
        return "key"
    }

}
