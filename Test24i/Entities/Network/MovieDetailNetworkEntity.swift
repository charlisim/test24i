//
//  MovieDetailNetworkEntity.swift
//
//  Created by Carlos on 26/8/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public struct MovieDetailNetworkEntity: Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let budget = "budget"
    static let backdropPath = "backdrop_path"
    static let revenue = "revenue"
    static let voteCount = "vote_count"
    static let overview = "overview"
    static let voteAverage = "vote_average"
    static let video = "video"
    static let imdbId = "imdb_id"
    static let id = "id"
    static let title = "title"
    static let homepage = "homepage"
    static let posterPath = "poster_path"
    static let adult = "adult"
    static let genres = "genres"
    static let status = "status"
    static let runtime = "runtime"
    static let originalTitle = "original_title"
    static let releaseDate = "release_date"
    static let originalLanguage = "original_language"
    static let popularity = "popularity"
    static let tagline = "tagline"

  }

  // MARK: Properties
  public var budget: Int?
  public var backdropPath: String?
  public var revenue: Int?
  public var voteCount: Int?
  public var overview: String?
  public var voteAverage: Int?
  public var video: Bool? = false
  public var imdbId: String?
  public var id: Int?
  public var title: String?
  public var homepage: String?
  public var posterPath: String?
  public var adult: Bool? = false
  public var status: String?
  public var runtime: Int?
  public var originalTitle: String?
  public var releaseDate: String?
  public var originalLanguage: String?
  public var popularity: Float?
  public var tagline: String?
    public var genres: [GenresNetworkEntity]?
  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public mutating func mapping(map: Map) {
    budget <- map[SerializationKeys.budget]
    backdropPath <- map[SerializationKeys.backdropPath]
    revenue <- map[SerializationKeys.revenue]
    voteCount <- map[SerializationKeys.voteCount]
    overview <- map[SerializationKeys.overview]
    voteAverage <- map[SerializationKeys.voteAverage]
    video <- map[SerializationKeys.video]
    imdbId <- map[SerializationKeys.imdbId]
    id <- map[SerializationKeys.id]
    title <- map[SerializationKeys.title]
    homepage <- map[SerializationKeys.homepage]
    posterPath <- map[SerializationKeys.posterPath]
    adult <- map[SerializationKeys.adult]
    status <- map[SerializationKeys.status]
    runtime <- map[SerializationKeys.runtime]
    originalTitle <- map[SerializationKeys.originalTitle]
    releaseDate <- map[SerializationKeys.releaseDate]
    originalLanguage <- map[SerializationKeys.originalLanguage]
    popularity <- map[SerializationKeys.popularity]
    tagline <- map[SerializationKeys.tagline]
    genres <- map[SerializationKeys.genres]

  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = budget { dictionary[SerializationKeys.budget] = value }
    if let value = backdropPath { dictionary[SerializationKeys.backdropPath] = value }
    if let value = revenue { dictionary[SerializationKeys.revenue] = value }
    if let value = voteCount { dictionary[SerializationKeys.voteCount] = value }
    if let value = overview { dictionary[SerializationKeys.overview] = value }
    if let value = voteAverage { dictionary[SerializationKeys.voteAverage] = value }
    dictionary[SerializationKeys.video] = video
    if let value = imdbId { dictionary[SerializationKeys.imdbId] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = homepage { dictionary[SerializationKeys.homepage] = value }
    if let value = posterPath { dictionary[SerializationKeys.posterPath] = value }
    dictionary[SerializationKeys.adult] = adult
    if let value = status { dictionary[SerializationKeys.status] = value }
    if let value = runtime { dictionary[SerializationKeys.runtime] = value }
    if let value = originalTitle { dictionary[SerializationKeys.originalTitle] = value }
    if let value = releaseDate { dictionary[SerializationKeys.releaseDate] = value }
    if let value = originalLanguage { dictionary[SerializationKeys.originalLanguage] = value }
    if let value = popularity { dictionary[SerializationKeys.popularity] = value }
    if let value = tagline { dictionary[SerializationKeys.tagline] = value }
    if let value = genres { dictionary[SerializationKeys.genres] = value }

    return dictionary
  }

}
