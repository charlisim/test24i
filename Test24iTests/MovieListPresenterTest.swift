//
//  MovieListPresenterTest.swift
//  Test24i
//
//  Created by Carlos on 27/8/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import XCTest
@testable import Test24i
import RxSwift

class MovieListPresenterTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    func testCallSetupView(){
        let view = MovieListViewStub()
        let output = OutputPresenter()
        let sut = MovieListPresenter(view: view, getMovies: GetMoviesEmptyMock(), searchMovies: SearchMoviesMock(), output: output)
        XCTAssertNotNil(sut.view)
        sut.didLoadView()
        XCTAssertEqual(view.setupViewCalledTimes, 1)
    }
    func testCallDidReceiveMovieListNotCalled(){
        let view = MovieListViewStub()
        let output = OutputPresenter()
        let sut = MovieListPresenter(view: view, getMovies: GetMoviesEmptyMock(), searchMovies: SearchMoviesMock(), output: output)
        XCTAssertNotNil(sut.view)
        sut.didLoadView()
        XCTAssertEqual(view.movieListCalledTimes, 0)
        XCTAssertEqual(view.showEmptyMoviesViewCalledTimes, 1)

    }
    
    func testCallDidReceiveMovieListCalled(){
        let view = MovieListViewStub()
        let output = OutputPresenter()
        let sut = MovieListPresenter(view: view, getMovies: GetMoviesWithDataMock(), searchMovies: SearchMoviesMock(), output: output)
        XCTAssertNotNil(sut.view)
        sut.didLoadView()
        XCTAssertEqual(view.movieListCalledTimes, 1)
        XCTAssertEqual(view.showEmptyMoviesViewCalledTimes, 0)
        
    }
    
    func testOutputCalledOnTapDetail(){
        let view = MovieListViewStub()
        let output = OutputPresenter()
        let sut = MovieListPresenter(view: view, getMovies: GetMoviesWithDataMock(), searchMovies: SearchMoviesMock(), output: output)
        XCTAssertNotNil(sut.view)
        sut.didSelectMovie(movie: Movie())
        XCTAssertEqual(output.goToDetailCalledTimes, 1)
    }
    
    
}
class GetMoviesEmptyMock: GetMovies{
    class MovieServiceMock: MovieService{
        func getDetailForMovie(_ movie: Movie) -> Observable<Movie> {
            return Observable.just(Movie())
        }
        func getPopularMovies() -> Observable<[Movie]> {
            return Observable.just([Movie()])
        }
        func getVideosForMovie(_ movie: Movie) -> Observable<Movie> {
            return Observable.just(Movie())
        }
    }
    class MoviePersistenceMock: MoviePresistenceService{
        func getDetailForMovie(_ movie: Movie) -> Observable<Movie> {
            return Observable.just(Movie())
        }
        func getPopularMovies() -> Observable<[Movie]> {
            return Observable.just([Movie()])
        }
        func getVideosForMovie(_ movie: Movie) -> Observable<Movie> {
            return Observable.just(Movie())
        }
        func saveMovie(movie: Movie) {
            
        }
        func saveMovies(movies: [Movie]) {
            
        }
    }
    init(){
        super.init(networkService: MovieServiceMock(), realmService: MoviePersistenceMock())
    }
    override func execute() -> Observable<[Movie]> {
        return Observable.just([])
    }
}

class GetMoviesWithDataMock: GetMovies{
    class MovieServiceMock: MovieService{
        func getDetailForMovie(_ movie: Movie) -> Observable<Movie> {
            return Observable.just(Movie())
        }
        func getPopularMovies() -> Observable<[Movie]> {
            return Observable.just([Movie()])
        }
        func getVideosForMovie(_ movie: Movie) -> Observable<Movie> {
            return Observable.just(Movie())
        }
    }
    class MoviePersistenceMock: MoviePresistenceService{
        func getDetailForMovie(_ movie: Movie) -> Observable<Movie> {
            return Observable.just(Movie())
        }
        func getPopularMovies() -> Observable<[Movie]> {
            return Observable.just([Movie()])
        }
        func getVideosForMovie(_ movie: Movie) -> Observable<Movie> {
            return Observable.just(Movie())
        }
        func saveMovie(movie: Movie) {
            
        }
        func saveMovies(movies: [Movie]) {
            
        }
    }
    init(){
        super.init(networkService: MovieServiceMock(), realmService: MoviePersistenceMock())
    }
    override func execute() -> Observable<[Movie]> {
        return Observable.just([Movie()])
    }
}

class SearchMoviesMock: SearchMovies{
    override func searchByTitle(text: String) -> [Movie] {
        return []
    }
}
class OutputPresenter: MovieListPresenterOutputProtocol{
    var goToDetailCalledTimes = 0
    func navigateToDetail(movie: Movie) {
        self.goToDetailCalledTimes += 1
    }
}
class MovieListViewStub: NSObject, MovieListView{
    var movieListCalledTimes = 0
    var showEmptyMoviesViewCalledTimes = 0
    var setupTitleCalledTimes = 0
    var setupViewCalledTimes = 0
    
    func setupView() {
        self.setupViewCalledTimes += 1
    }
    
    func didReceiveMovieList(movies:[Movie]){
        self.movieListCalledTimes += 1
    }
    func showEmptyMoviesView(){
        self.showEmptyMoviesViewCalledTimes += 1
    }
    func setupTitle(title:String){
        self.setupTitleCalledTimes += 1
    }
}
