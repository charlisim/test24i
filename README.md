# How is structured #

App is structured following the clean code architecture and views are build with MVP (Model - View - Presenter).

The app flow starts with the RootCoordinator, the responsability of coordinators is to control the navigation flow of the application.
RootCoordinator call a feature builder that builds views and connect it with the presenter, passing all the dependencies. 
The point is that all views and presenters don't create any object, all objects needed are passed as parameter and are build in a single point, this allows code reusability and increase testability.

I have used MVP to avoid the massive view controllers, Views are defined by a protocol that will be implemented by a view controller, this protocol only defines methods that send information that needs to be shown.
For example `showMessageError(message:String)`. This methods are called by the presenter linked to this view that is the class that control the flow of the view and reacts to actions send by the view, like when a button is tapped, and make the needed actions, like make a network call.
To improve the reusability of network calls in different points of the application and allow abstraction of the source of the data, i.e. presenters not need to know if the data comes from the network, a cache or a database, I have used Use Cases, that coordinate between different sources of data, like network or database, depending of the business logic.

Use cases uses services, whose responsibility is to make the calls and get the data from each source and transform it from source format to app model format. For example MoviesNetworkService, have different methods to call each URL and when data is received is transformed from MovieNetworkEntity, where data is in the same format that JSON downloaded, to Movie model that will be used in all app.
This separation between Entities and Models is done to isolate each source of data, and limit the impact of a change for example between two databases libraries, in this demo I've used Realm but we could use Core Data only by changing the object passed to the use case. This also increase testability because allows to be changed with a test double(mock, stub...) easily.

I've used RxSwift to handle all async calls, this library allows to get rid of the callbacks, that produce unmaintainable code, is similar to promises but more powerful in certain scenarios like when you have a steam of data for example for geolocation.

I didn't used Storyboard or Xibs, and I did all the layout by code using an extension that simplifies the use of NSLayoutConstraints. 
The reason to use code for autolayout is that simplifies changes and allows reusability of views because the same view can react to different parameters and get transformed in way that even that can achieved with storyboards I prefer to have all the layout logic in the same place, because if you have constraints in storyboards and code you have trouble to know what constraints are applied. 

