//
//  UIBaseView.swift
//  Test24i
//
//  Created by Carlos on 26/8/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import Foundation


public protocol UIBaseView: NSObjectProtocol {
    func setupView()
}

public extension UIBaseView where Self:SetupUI{
    func setupView(){
        self.setupViews()
        self.setupConstraints()
        self.setupActions()
    }
}
