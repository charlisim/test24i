//
//  UIBasePresenter.swift
//  Test24i
//
//  Created by Carlos on 26/8/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import Foundation


public protocol UIBasePresenter {
   
    func didLoadView()
    func onViewAppeared()
    func onViewDissapear()
}
