//
//  SetupUI.swift
//  Test24i
//
//  Created by Carlos on 26/8/17.
//  Copyright © 2017 csimon. All rights reserved.
//

import UIKit


public protocol SetupUI{
    func setupViews()
    func setupConstraints()
    func setupActions()
}


public extension SetupUI{
    func setupActions() { }
}
